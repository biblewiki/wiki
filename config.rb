#!/usr/bin/env ruby
require 'rubygems'
require 'rack/cas'
# you may set this line after you change the default git adapter if needed
require 'gollum/app'

# I use external config file settings
gollum_config_file = File.dirname(File.expand_path(__FILE__)) + '/config_gollum.yml'
$my_settings = Hash.new { |hash, key| hash[key] = '' }
if File.readable?(gollum_config_file) then
  $my_settings = YAML.load_file(gollum_config_file)
end

Precious::App.set(:gollum_path, $my_settings[:wiki_path])
Precious::App.set(:wiki_options,$my_settings[:wiki_options])

sanitizer = Gollum::Sanitization.new
sanitizer.attributes['div'].push 'style' # Attributes
Precious::App.set(:wiki_options, {sanitization: sanitizer})

use Rack::Session::Cookie,
        :key => 'rack.session', :path => '/',
        :secret => Digest::SHA2.file(gollum_config_file).hexdigest # lol
use Rack::CAS, server_url: $my_settings[:sso]

module Precious
  class App < Sinatra::Base
    ['/create/*','/edit/*'].each do |path|
      before path do
        unless session['cas'] && session['cas']['user']
          halt 401, 'Unauthorized'
        end
        unless $my_settings[:writers] && 
                $my_settings[:writers].index(session['cas']['user'])
          halt 403, 'Forbidden write acces for ' + session['cas']['user']
        end

        session['gollum.author'] = {:name => "#{eval $my_settings[:name]}",
                                    :email => "#{eval $my_settings[:email]}"}
      end
    end
  end
end

run Precious::App

